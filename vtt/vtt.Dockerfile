FROM node:alpine


# Update the repository and install SteamCMD
# ARG DEBIAN_FRONTEND=noninteractive



# RUN mkdir linuxgsm
# RUN chmod 777 linuxgsm

# USER linuxgsm

# WORKDIR linuxgsm
# RUN wget -O linuxgsm.sh https://linuxgsm.sh
# RUN chmod +x linuxgsm.sh
# RUN bash linuxgsm.sh tf2server
# RUN ./tf2server auto-install

# COPY cfg_files/* /linuxgsm/serverfiles/tf/cfg/
# COPY html/maps/* /linuxgsm/serverfiles/tf/maps/

RUN mkdir /app
RUN wget -O foundryvtt.zip https://s3.us-west-1.wasabisys.com/mredig-fileshare/foundryvtt.zip
RUN unzip foundryvtt.zip -d /app



# WORKDIR /linuxgsm/serverfiles
RUN mkdir /data
WORKDIR /app

EXPOSE 80
ENTRYPOINT ["node", "resources/app/main.js", "--dataPath=/data"]
