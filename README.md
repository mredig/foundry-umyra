# Umyra Foundry VTT

### Requirements
* a Linux server running Docker
* dev claims needs 2 gigs of memory, but doesn't appear to need that much

### Getting Started
Deploy a server on Vultr with the following script:

```bash
#!/usr/bin/env bash

export DUCKDNSHOST=umyravtt
export DUCKDNSTOKEN=[insert duck dns token no brackets]
export AUTOSTART=true

curl -o init.sh https://gitlab.com/mredig/foundry-umyra/-/raw/main/setup_foundry.sh
chmod +x init.sh
./init.sh


```

Server specs don't need to be extravagent. You can probably get away with the $3.50 server, but since hourly is cheap you might as well get a $12-100 server.

#### Maintenance
The server file is hosted at https://s3.us-west-1.wasabisys.com/mredig-fileshare/foundryvtt.zip