#!/usr/bin/env bash

# downloads and runs a script in my `vm-init-scripts` repo hosted on gitlab. Additional parameters are passed to the script as arguments
loadAndRunInitScript () {
	PARAMS=($@) # copy $@ to PARAMS
	unset PARAMS[0] # remove the script name from args passed to the script
	SCRIPT=$1 # store the script name

	curl -o $SCRIPT.sh "https://gitlab.com/mredig/vm-init-scripts/-/raw/main/$SCRIPT.sh"
	chmod +x $SCRIPT.sh
	./$SCRIPT.sh ${PARAMS[*]} # run the script, passing all its args
	rm $SCRIPT.sh
}

echo "+++=========DYN DUCK DNS===========+++"
curl "https://www.duckdns.org/update?domains=$DUCKDNSHOST&token=$DUCKDNSTOKEN"
echo "updated dyn duck ip"

echo "+++=========HOME ROOT SETUP===========+++"
su root
cd /root
export HOME=/root

echo "export IS_STAGING=$IS_STAGING" >> .env
echo "export DUCKDNSHOST=$DUCKDNSHOST" >> .env
echo "export DUCKDNSTOKEN=$DUCKDNSTOKEN" >> .env
echo "export AUTOSTART=$AUTOSTART" >> .env
chmod 700 .env
echo "source ~/.env" >> ~/.bashrc


echo "+++=========SSH SETUP===========+++"
loadAndRunInitScript "install_ssh_keys"
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCsTsw9pxN4vFUZg49CASvTw2if2a9u8Ez4KwrmENc71TFaiP0nWxYHs9CK6xWAbL0HXNYJPQl11foXdtS6jAOG514Sv2YOE7PtYAWOA/ThHD8tS2nq/nDHTwnm/141DDgcJeV2dwfqXf6RD4bDJ5NDM3KfON0Bra1zXB0XrYSIfwVgAMwnyOvGhsVa8xhqyGr6NoBN+E12iBfmHGAqosV2Psx3DuFOP137H+GKFGkXZg/xElDjZxBYBSWDgx3cs4tvkpPQ0weKCSg2EiqflIpYFf8B0KzZk1Za8f19wjj7gSNSs38HHF/oxmeT/47fjwrHEU0P0ZHuihDIrOkGaCegeU6kFTla74VTHMjfX0DcYAZpyfywWRG4cRx70Y4OQPTJym+xZZ7th6G77AW3339K2yV/IwnSPdeBsi6otEZhD5rtzwgUnYyh+Z29OJXGw2c5GjrvOstw0exslDZ0JjwqEvekwb8I/5ier5u8cDUxjQbE1BC81LuA9Knr+9JKQJkD3VNahfjh+4jSaXoJR5kslJ/Wjz/op23PzMueueMu0Dnp0nzvza8ivThIuvuvxpgE2jP4Qtj5oSU4cwvz6TRG25TuDTOSy8jJZFcZe8Igdr0OD3+STRBX/IUnA2+bxBAeAYzwtOGACnvDmOt3QjDrVsIIRtzkvc1Pv5To4xVImw==" >> ~/.ssh/authorized_keys

echo "+++=========UPDATES AND GIT SETUP===========+++"
loadAndRunInitScript "apt_update_and_install" "git" "git-lfs"
git config --global user.email "he@ho.hum"
git config --global user.name "Umyra Cloud Server"
git lfs install

echo "+++=========DOCKER SETUP===========+++"
loadAndRunInitScript "install_docker"
source ~/.bashrc

echo "+++=========DEDICATED SERVER COMPOSE SETUP===========+++"
git clone https://gitlab.com/mredig/foundry-umyra.git
cd foundry-umyra
docker compose build
if [[ $AUTOSTART == "true" ]]
then 
	docker compose up -d 

	while [ -z "$(docker compose logs certbot | grep "Successfully received certificate.")" ]; 
	do
		echo "Waiting for certbot to finish..."
		sleep 10;
	done
fi

echo "+++=========FIX SSH===========+++"
cd /etc/ssh

sed 's/#ListenAddress 0/ListenAddress 0/g' sshd_config > sshd_config.2
mv sshd_config.2 sshd_config

reboot # usually some updates require an initial reboot