#!/usr/bin/env sh

while true; do inotifywait -e modify /etc/nginx/conf.d/default.conf; nginx -s reload; done &