FROM nginx:alpine

RUN apk add inotify-tools

COPY conf_watcher.sh /docker-entrypoint.d/40-conf_watcher.sh