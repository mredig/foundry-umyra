#!/usr/bin/env sh

COMMAND="
certbot certonly \
--webroot \
--webroot-path /var/www/certbot/ \
-d umyra.secretgamegroup.com \
--non-interactive \
--agree-tos \
-m clime.wiser_0j@icloud.com"

if [[ $IS_STAGING == "true" ]]; then
	COMMAND="
	certbot certonly \
	--test-cert \
	--webroot \
	--webroot-path /var/www/certbot/ \
	-d umyra.secretgamegroup.com \
	--non-interactive \
	--agree-tos \
	-m clime.wiser_0j@icloud.com"
fi


while true; 
do
	$COMMAND

	if [ $? -eq 0 ]; then
		cd /etc/nginx/conf.d/
		sed 's/^#//g' default.conf > default.conf.tmp
		mv default.conf.tmp default.conf
	
		sleep 86400
	else
		sleep 30
	fi
done