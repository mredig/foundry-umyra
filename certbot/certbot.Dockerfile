FROM certbot/certbot:latest

COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]